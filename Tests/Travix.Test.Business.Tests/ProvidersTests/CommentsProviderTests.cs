using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Z.EntityFramework.Plus;
using Travix.Test.Data.UnitOfWork;
using Travix.Test.Common.TypeMapping;
using Travix.Test.Common.Exceptions.Api;
using Travix.Test.Business.Providers;
using Travix.Test.Business.Models;
using Travix.Test.Business.Providers.Interfaces;

namespace Travix.Test.Business.Tests.ProvidersTests
{
    [TestFixture]
    public class CommentsProviderTests
    {
        private Fixture _fixture;
        private IMapper _mapper;
        private TestUnitOfWork _testUnitOfWork;
        private ICommentsProvider _testProvider;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _fixture = new Fixture();
            _fixture.Customize<Comment>(x => x.Without(b => b.PostId));
            _fixture.Customize<Data.Entities.Comment>(x => x.Without(b => b.Post));

            // Mocking of Mapper class is an item for holy war, but I've decided to use real instance in unit tests,
            // this do tests more clear and simple for understand and support (let's consider that is system class).
            _mapper = new Mapper();

            // So, here is no sample of using mocks, but I'm experienced with Moq library.
        }

        [SetUp]
        public void SetUp()
        {
            // In-memory database is a recommended way in .Net Core for tests
            var options = new DbContextOptionsBuilder<TestUnitOfWork>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            BatchDeleteManager.InMemoryDbContextFactory = () => new TestUnitOfWork(options);
            _testUnitOfWork = new TestUnitOfWork(options);

            // Generate several test posts with comments
            for (int i = 1; i < 4; i++)
            {
                var dbEntities = _fixture.CreateMany<Data.Entities.Comment>().ToList();
                foreach (var dbEntity in dbEntities)
                    dbEntity.PostId = i;
                _testUnitOfWork.Comments.AddRange(dbEntities);
            }
            _testUnitOfWork.SaveChanges();

            _testProvider = new CommentsProvider(_testUnitOfWork, _mapper);
        }

        [Test]
        public async Task Should_return_all_comments_for_post()
        {
            // Arrange
            var testPostId = (await _testUnitOfWork.Comments.All.LastAsync()).PostId;
            var expectedPostComments = await _testUnitOfWork.Comments.All.Where(x => x.PostId == testPostId).ToListAsync();

            // Act
            var result = await _testProvider.GetAllCommentsForPost(testPostId);

            // Assert
            Assert.AreEqual(result.Count, expectedPostComments.Count);
            CollectionAssert.AreEqual(expectedPostComments.Select(x => x.PostId), result.Select(x => x.PostId));
            CollectionAssert.AreEqual(expectedPostComments.Select(x => x.CommentId), result.Select(x => x.CommentId));
            CollectionAssert.AreEqual(expectedPostComments.Select(x => x.Text), result.Select(x => x.Text));
        }

        [Test]
        public async Task Should_get_comment_by_id()
        {
            // Arrange
            var expectedEntity = await _testUnitOfWork.Comments.All.LastAsync();

            // Act
            var result = await _testProvider.GetComment(expectedEntity.CommentId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedEntity.PostId, result.PostId);
            Assert.AreEqual(expectedEntity.CommentId, result.CommentId);
            Assert.AreEqual(expectedEntity.Text, result.Text);
        }

        [Test]
        public void Should_create_new_comment_for_post()
        {
            // Arrange
            var model = _fixture.Create<Comment>();

            // Act
            var result = _testProvider.Create(model).Result;

            // Assert
            var dbEntity = _testUnitOfWork.Comments.All.First(x => x.CommentId == result.CommentId);
            Assert.IsNotNull(dbEntity);
            Assert.AreEqual(model.PostId, dbEntity.PostId);
            Assert.AreEqual(model.Text, dbEntity.Text);
        }

        [Test]
        public void Should_return_correct_model_after_create_new_comment()
        {
            // Arrange
            var model = _fixture.Create<Comment>();

            // Act
            var result = _testProvider.Create(model).Result;

            // Assert
            Assert.AreNotEqual(result.CommentId, 0);
            Assert.AreEqual(model.PostId, result.PostId);
            Assert.AreEqual(model.Text, result.Text);
        }

        [Test]
        public async Task Should_update_existing_comment()
        {
            // Arrange
            var dbEntity = await _testUnitOfWork.Comments.All.LastAsync();
            var updateModel = _fixture.Create<Comment>();
            updateModel.PostId = dbEntity.PostId;
            updateModel.CommentId = dbEntity.CommentId;

            // Act
            await _testProvider.Update(updateModel);

            // Assert
            var result = _testUnitOfWork.Comments.All.FirstOrDefault(x => x.CommentId == dbEntity.CommentId);
            Assert.IsNotNull(result);
            Assert.AreEqual(updateModel.PostId, result.PostId);
            Assert.AreEqual(updateModel.Text, result.Text);
        }

        [Test]
        public void Should_throw_not_found_exception_when_update_not_existing_comment()
        {
            // Arrange
            var wrongComment = _fixture.Create<Comment>();

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _testProvider.Update(wrongComment));
        }

        [Test]
        public async Task Should_delete_comment()
        {
            // Arrange
            var dbEntity = await _testUnitOfWork.Comments.All.LastAsync();

            // Act
            await _testProvider.Delete(dbEntity.CommentId);

            // Assert
            var result = _testUnitOfWork.Comments.All.FirstOrDefault(x => x.CommentId == dbEntity.CommentId);
            Assert.IsNull(result);
        }

        [Test]
        public void Should_throw_not_found_exception_when_delete_not_existing_comment()
        {
            // Arrange
            var wrongId = _fixture.Create<int>();

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _testProvider.Delete(wrongId));
        }
    }
}