using System;
using System.Linq;
using System.Threading.Tasks;
using AutoFixture;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using Z.EntityFramework.Plus;
using Travix.Test.Data.UnitOfWork;
using Travix.Test.Common.TypeMapping;
using Travix.Test.Common.Exceptions.Api;
using Travix.Test.Business.Providers;
using Travix.Test.Business.Models;
using Travix.Test.Business.Providers.Interfaces;

namespace Travix.Test.Business.Tests.ProvidersTests
{
    [TestFixture]
    public class PostsProviderTests
    {
        private Fixture _fixture;
        private IMapper _mapper;
        private TestUnitOfWork _testUnitOfWork;
        private IPostsProvider _testProvider;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            _fixture = new Fixture();
            _fixture.Customize<Post>(x => x.Without(b => b.PostId));
            _fixture.Customize<Data.Entities.Post>(x => x.Without(b => b.PostComments));

            // Mocking of Mapper class is an item for holy war, but I've decided to use real instance in unit tests,
            // this do tests more clear and simple for understand and support (let's consider that is system class).
            _mapper = new Mapper();

            // So, here is no sample of using mocks, but I'm experienced with Moq library.
        }

        [SetUp]
        public void SetUp()
        {
            // In-memory database is a recommended way in .Net Core for tests
            var options = new DbContextOptionsBuilder<TestUnitOfWork>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;
            BatchDeleteManager.InMemoryDbContextFactory = () => new TestUnitOfWork(options);
            _testUnitOfWork = new TestUnitOfWork(options);

            // Test entities
            var dbEntities = _fixture.CreateMany<Data.Entities.Post>(5);
            _testUnitOfWork.Posts.AddRange(dbEntities);
            _testUnitOfWork.SaveChanges();

            _testProvider = new PostsProvider(_testUnitOfWork, _mapper);
        }

        [Test]
        public async Task Should_return_all_posts()
        {
            // Arrange
            var expectedEntities = await _testUnitOfWork.Posts.All.ToListAsync();

            // Act
            var result = await _testProvider.GetAllPosts();

            // Assert
            Assert.AreEqual(result.Count, expectedEntities.Count);
            CollectionAssert.AreEqual(expectedEntities.Select(x => x.PostId), result.Select(x => x.PostId));
            CollectionAssert.AreEqual(expectedEntities.Select(x => x.Title), result.Select(x => x.Title));
            CollectionAssert.AreEqual(expectedEntities.Select(x => x.Body), result.Select(x => x.Body));
        }

        [Test]
        public async Task Should_get_post_by_id()
        {
            // Arrange
            var expectedEntity = await _testUnitOfWork.Posts.All.LastAsync();

            // Act
            var result = await _testProvider.GetPost(expectedEntity.PostId);

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual(expectedEntity.PostId, result.PostId);
            Assert.AreEqual(expectedEntity.Title, result.Title);
            Assert.AreEqual(expectedEntity.Body, result.Body);
        }

        [Test]
        public void Should_create_new_post()
        {
            // Arrange
            var model = _fixture.Create<Post>();

            // Act
            var result = _testProvider.Create(model).Result;

            // Assert
            var dbEntity = _testUnitOfWork.Posts.All.First(x => x.PostId == result.PostId);
            Assert.IsNotNull(dbEntity);
            Assert.AreEqual(model.Title, dbEntity.Title);
            Assert.AreEqual(model.Body, dbEntity.Body);
        }

        [Test]
        public void Should_return_correct_model_after_create_new_post()
        {
            // Arrange
            var model = _fixture.Create<Post>();

            // Act
            var result = _testProvider.Create(model).Result;

            // Assert
            Assert.AreNotEqual(result.PostId, 0);
            Assert.AreEqual(model.Title, result.Title);
            Assert.AreEqual(model.Body, result.Body);
        }

        [Test]
        public async Task Should_update_existing_post()
        {
            // Arrange
            var dbEntity = await _testUnitOfWork.Posts.All.LastAsync();
            var updateModel = _fixture.Create<Post>();
            updateModel.PostId = dbEntity.PostId;

            // Act
            await _testProvider.Update(updateModel);

            // Assert
            var result = _testUnitOfWork.Posts.All.FirstOrDefault(x => x.PostId == dbEntity.PostId);
            Assert.IsNotNull(result);
            Assert.AreEqual(updateModel.Title, result.Title);
            Assert.AreEqual(updateModel.Body, result.Body);
        }

        [Test]
        public void Should_throw_not_found_exception_when_update_not_existing_post()
        {
            // Arrange
            var wrongPost = _fixture.Create<Post>();

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _testProvider.Update(wrongPost));
        }

        [Test]
        public async Task Should_delete_post()
        {
            // Arrange
            var dbEntity = await _testUnitOfWork.Posts.All.LastAsync();

            // Act
            await _testProvider.Delete(dbEntity.PostId);

            // Assert
            var result = _testUnitOfWork.Posts.All.FirstOrDefault(x => x.PostId == dbEntity.PostId);
            Assert.IsNull(result);
        }

        [Test]
        public void Should_throw_not_found_exception_when_delete_not_existing_post()
        {
            // Arrange
            var wrongId = _fixture.Create<int>();

            // Act / Assert
            Assert.ThrowsAsync<NotFoundException>(async () => await _testProvider.Delete(wrongId));
        }
    }
}