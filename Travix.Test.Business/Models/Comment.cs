﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Travix.Test.Common.Models;

namespace Travix.Test.Business.Models
{
    public class Comment : IEntity
    {
        [Required]
        public int CommentId { get; set; }

        [Required]
        public int PostId { get; set; }

        [Required, StringLength(256)]
        public string Text { get; set; }
    }
}
