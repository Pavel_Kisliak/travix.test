using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Travix.Test.Business.Models
{
    public class Post
    {
        [Required]
        public int PostId { get; set; }

        [Required, StringLength(255)]
        public string Title { get; set; }

        [Required, StringLength(4096)]
        public string Body { get; set; }
    }
}