﻿using System.Runtime.CompilerServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Travix.Test.Business.Providers;
using Travix.Test.Business.Providers.Interfaces;
using Travix.Test.Common;

[assembly: InternalsVisibleTo("Travix.Test.Business.Tests")]
namespace Travix.Test.Business
{
    public class Bootstrapper : IBootstrapper
    {
        /// <inheritdoc />
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            // Register options
            // ...

            // Register services
            services.AddScoped<IPostsProvider, PostsProvider>();
            services.AddScoped<ICommentsProvider, CommentsProvider>();
        }
    }
}
