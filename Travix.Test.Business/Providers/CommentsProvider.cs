using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using Travix.Test.Business.Models;
using Travix.Test.Common.TypeMapping;
using Travix.Test.Data.UnitOfWork;
using Travix.Test.Business.Providers.Interfaces;
using Travix.Test.Common.Exceptions.Api;

namespace Travix.Test.Business.Providers
{
    class CommentsProvider : ICommentsProvider
    {
        private readonly ITestUnitOfWork _testUnitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="CommentsProvider"/> class.
        /// </summary>
        /// <param name="testUnitOfWork">unit of work.</param>
        /// <param name="mapper">The mapper.</param>
        public CommentsProvider(ITestUnitOfWork testUnitOfWork, IMapper mapper)
        {
            _testUnitOfWork = testUnitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<IList<Comment>> GetAllCommentsForPost(int postId)
        {
            var entities = await _testUnitOfWork.Comments.All
                .Where(x => x.PostId == postId)
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<Comment[]>(entities);
        }

        /// <inheritdoc />
        public async Task<Comment> GetComment(int id)
        {
            var entity= await _testUnitOfWork.Comments.All
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.CommentId == id);

            return _mapper.Map<Comment>(entity);
        }

        /// <inheritdoc />
        public async Task<Comment> Create(Comment comment)
        {
            var dbEntity = _mapper.Map<Data.Entities.Comment>(comment);
            _testUnitOfWork.Comments.Add(dbEntity);
            await _testUnitOfWork.SaveAsync();
            return _mapper.Map<Comment>(dbEntity);
        }

        /// <inheritdoc />
        public async Task Update(Comment comment)
        {
            var entity = await _testUnitOfWork.Comments.All
                .SingleOrDefaultAsync(x => x.CommentId == comment.CommentId);
            if (entity == null)
            {
                throw new NotFoundException($"Comment with id '{comment.CommentId}' does not exist.");
            }
            _mapper.Map(comment, entity);
            await _testUnitOfWork.SaveAsync();
        }

        /// <inheritdoc />
        public async Task Delete(int id)
        {
            var result = await _testUnitOfWork.Comments.All
                .Where(x => x.CommentId == id)
                .DeleteAsync();
            if (result == 0)
            {
                throw new NotFoundException($"Comment with id '{id}' does not exist.");
            }
        }
    }
}