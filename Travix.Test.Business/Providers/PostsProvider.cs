using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Z.EntityFramework.Plus;
using Travix.Test.Business.Models;
using Travix.Test.Common.TypeMapping;
using Travix.Test.Data.UnitOfWork;
using Travix.Test.Business.Providers.Interfaces;
using Travix.Test.Common.Exceptions.Api;

namespace Travix.Test.Business.Providers
{
    class PostsProvider : IPostsProvider
    {
        private readonly ITestUnitOfWork _testUnitOfWork;
        private readonly IMapper _mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="PostsProvider"/> class.
        /// </summary>
        /// <param name="testUnitOfWork">The unit of work.</param>
        /// <param name="mapper">The mapper.</param>
        public PostsProvider(ITestUnitOfWork testUnitOfWork, IMapper mapper)
        {
            _testUnitOfWork = testUnitOfWork;
            _mapper = mapper;
        }

        /// <inheritdoc />
        public async Task<IList<Post>> GetAllPosts()
        {
            var entities = await _testUnitOfWork.Posts.All
                .AsNoTracking()
                .ToListAsync();

            return _mapper.Map<Post[]>(entities);
        }

        /// <inheritdoc />
        public async Task<Post> GetPost(int id)
        {
            var entity= await _testUnitOfWork.Posts.All
                .AsNoTracking()
                .FirstOrDefaultAsync(x => x.PostId == id);

            return _mapper.Map<Post>(entity);
        }

        /// <inheritdoc />
        public async Task<Post> Create(Post post)
        {
            var dbEntity = _mapper.Map<Data.Entities.Post>(post);
            _testUnitOfWork.Posts.Add(dbEntity);
            await _testUnitOfWork.SaveAsync();
            return _mapper.Map<Post>(dbEntity);
        }

        /// <inheritdoc />
        public async Task Update(Post post)
        {
            var entity = await _testUnitOfWork.Posts.All
                .SingleOrDefaultAsync(x => x.PostId == post.PostId);
            if (entity == null)
            {
                throw new NotFoundException($"Post with id '{post.PostId}' does not exist.");
            }
            _mapper.Map(post, entity);
            await _testUnitOfWork.SaveAsync();
        }

        /// <inheritdoc />
        public async Task Delete(int id)
        {
            var result = await _testUnitOfWork.Posts.All
                .Where(x => x.PostId == id)
                .DeleteAsync();
            if (result == 0)
            {
                throw new NotFoundException($"Post with id '{id}' does not exist.");
            }
        }
    }
}