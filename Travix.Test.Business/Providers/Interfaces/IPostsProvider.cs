﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travix.Test.Business.Models;

namespace Travix.Test.Business.Providers.Interfaces
{
    public interface IPostsProvider
    {
        /// <summary>
        /// Gets all posts.
        /// </summary>
        /// <returns></returns>
        Task<IList<Post>> GetAllPosts();

        /// <summary>
        /// Gets post by ID.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Post> GetPost(int id);

        /// <summary>
        /// Creates new post.
        /// </summary>
        /// <param name="post">The post.</param>
        /// <returns></returns>
        Task<Post> Create(Post post);

        /// <summary>
        /// Updates the existing post.
        /// </summary>
        /// <param name="post">The post.</param>
        /// <returns></returns>
        Task Update(Post post);

        /// <summary>
        /// Deletes the specified post.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task Delete(int id);
    }
}
