﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Travix.Test.Business.Models;

namespace Travix.Test.Business.Providers.Interfaces
{
    public interface ICommentsProvider
    {
        /// <summary>
        /// Gets all comments for post.
        /// </summary>
        /// <param name="postId">The post identifier.</param>
        /// <returns></returns>
        Task<IList<Comment>> GetAllCommentsForPost(int postId);

        /// <summary>
        /// Gets the comment by ID.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Comment> GetComment(int id);

        /// <summary>
        /// Creates new comment.
        /// </summary>
        /// <param name="comment">The comment.</param>
        /// <returns></returns>
        Task<Comment> Create(Comment comment);

        /// <summary>
        /// Updates the existing comment.
        /// </summary>
        /// <param name="comment">The comment.</param>
        /// <returns></returns>
        Task Update(Comment comment);

        /// <summary>
        /// Deletes the specified comment.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task Delete(int id);
    }
}
