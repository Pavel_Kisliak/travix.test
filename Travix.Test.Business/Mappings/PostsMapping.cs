using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using Travix.Test.Business.Models;
using Travix.Test.Common.TypeMapping;
using DataEntities = Travix.Test.Data.Entities;

namespace Travix.Test.Business.Mappings
{
    internal class PostsMapping : MapProfileBase
    {
        public PostsMapping()
        {
            CreateMap<Post, DataEntities.Post>()
                .ForMember(x => x.PostComments, opt => opt.Ignore())
                .ReverseMap();
            CreateMap<Comment, DataEntities.Comment>()
                .ForMember(x => x.Post, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}