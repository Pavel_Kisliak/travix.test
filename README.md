## Test task for Travix project.

---

## Requirements:

1. Visual Studio 2017 / .Net Core 2.0
2. SQL Server 2016

---

## How to run:

1. Open solution file in "Travix.Test.sln" Visual Studio.
2. Update ConnectionString in file "appsettings.json" if your installed instance of SQL Server isn't "(local)".
3. Compile and run on preferable server (IIS express or Kestrel, can be selected in dropdown near the Run button).

If connection strings was defined correctly, application automatically create test database named "TravixTest" and after then open Swagger API page in your browser.
