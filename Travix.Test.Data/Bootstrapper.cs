﻿using System.Reflection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Travix.Test.Common;
using Travix.Test.Data.UnitOfWork;

namespace Travix.Test.Data
{
    public class Bootstrapper : IBootstrapper
    {
        /// <inheritdoc />
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            // AddDbContext or AddDbContextPool for increase performance
            services.AddDbContextPool<TestUnitOfWork>(options =>
                options.UseSqlServer(configuration.GetConnectionString("TravixTestDatabase"),
                    b => b.MigrationsAssembly(typeof(TestUnitOfWork).GetTypeInfo().Assembly.GetName().Name))
            );
            // Register the service and implementation for the database context
            services.AddScoped<ITestUnitOfWork>(provider => provider.GetService<TestUnitOfWork>());
        }
    }
}
