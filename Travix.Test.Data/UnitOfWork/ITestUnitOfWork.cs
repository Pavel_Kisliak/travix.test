﻿using System;
using System.Collections.Generic;
using System.Text;
using Travix.Test.Common.Repositories;
using Travix.Test.Data.Entities;

namespace Travix.Test.Data.UnitOfWork
{
    public interface ITestUnitOfWork : IUnitOfWorkBase
    {
        IRepository<Post> Posts { get; }
        IRepository<Comment> Comments { get; }
    }
}
