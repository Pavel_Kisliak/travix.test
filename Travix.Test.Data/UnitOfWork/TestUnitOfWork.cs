﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Travix.Test.Common.Repositories;
using Travix.Test.Data.Entities;

namespace Travix.Test.Data.UnitOfWork
{
    public class TestUnitOfWork : UnitOfWorkBase, ITestUnitOfWork
    {
        public TestUnitOfWork(DbContextOptions<TestUnitOfWork> options)
            : base(options)
        {
        }

        public IRepository<Post> Posts => GetRepository<Post>();
        public IRepository<Comment> Comments => GetRepository<Comment>();

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.HasDefaultSchema("dbo");

            // Entities need to be registered explicitly, because IRepository doesn't do that
            modelBuilder.Entity<Post>();
            modelBuilder.Entity<Comment>();
        }
    }
}
