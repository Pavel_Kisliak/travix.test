﻿using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Travix.Test.Data.Entities;
using Travix.Test.Data.UnitOfWork;

namespace Travix.Test.Data
{
    public static class DbInitializer
    {
        public static void Initialize(TestUnitOfWork context, IHostingEnvironment hostingEnvironment)
        {
            // For outside migrations (can't mix this with migrations)
            //context.Database.EnsureCreated();

            // For code migrations (PMC or CLI generated)
            context.Database.Migrate();

            InitReferenceData(context);
            if (hostingEnvironment.IsDevelopment())
            {
                InitDevTestData(context);
            }
        }

        private static void InitReferenceData(TestUnitOfWork context)
        {
            // context.SaveChanges();
        }

        private static void InitDevTestData(TestUnitOfWork context)
        {
            if (context.Posts.All.Any())
            {
                return; // DB has been seeded
            }

            var post = new Post
            {
                Title = "Google-Nest merger raises privacy issues",
                Body =
@"Tech giant Alphabet is merging its Google and Nest divisions together.
The firm suggests the move will aid its efforts to build hardware and software to 'create a more thoughtful home'.
Nest had run as a standalone unit since its $3.2bn(£2.3bn) takeover in 2014.Its smart home products benefit from gathering data about its users.
Nest previously pledged the data would be kept separate from Google's other operations. Privacy campaigners have raised concerns at the reorganisation.
But Google has said it will be 'transparent' about any changes that might be made.",
                PostComments =
                {
                    new Comment { Text = "Comment 1 for post 1" },
                    new Comment { Text = "Comment 2 for post 1" }
                }
            };
            context.Posts.Add(post);
            var post2 = new Post
            {
                Title = "EE 'shoebox' to tackle broadband not-spots",
                Body =
@"Telecoms company EE has revealed plans to sell a 4G antenna that promises to bring fast broadband internet to thousands of homes in rural areas.
EE's Simon Till said the 'shoebox - size' antenna would let more people access the company's 4G broadband service.
The company said it had delivered speeds of 100Mbps to homes during a trial in Cumbria.",
                PostComments =
                {
                    new Comment { Text = "Comment 1 for post 2" },
                    new Comment { Text = "Comment 2 for post 2" }
                }
            };
            context.Posts.Add(post2);

            context.SaveChanges();
        }
    }
}
