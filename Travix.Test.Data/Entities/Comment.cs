﻿using System;
using System.Collections.Generic;
using System.Text;
using Travix.Test.Common.Models;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Travix.Test.Data.Entities
{
    public class Comment : IEntity
    {
        [Key]
        public int CommentId { get; set; }
        [Required]
        public int PostId { get; set; }

        [Required, StringLength(256)]
        public string Text { get; set; }

        [ForeignKey(nameof(PostId))]
        public virtual Post Post { get; set; }
}
}
