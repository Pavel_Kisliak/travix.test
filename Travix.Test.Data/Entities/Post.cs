using System;
using System.Collections.Generic;
using System.Text;
using Travix.Test.Common.Models;
using System.ComponentModel.DataAnnotations;

namespace Travix.Test.Data.Entities
{
    public class Post : IEntity
    {
        [Key]
        public int PostId { get; set; }

        [Required, StringLength(255)]
        public string Title { get; set; }

        [Required, StringLength(4096)]
        public string Body { get; set; }

        public virtual ICollection<Comment> PostComments { get; set; } = new HashSet<Comment>();
    }
}