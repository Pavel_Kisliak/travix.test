﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Travix.Test.Common.TypeMapping;

namespace Travix.Test.Common
{
    public class Bootstrapper : IBootstrapper
    {
        /// <inheritdoc />
        public void ConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.AddSingleton<IMapper, Mapper>();
        }
    }
}
