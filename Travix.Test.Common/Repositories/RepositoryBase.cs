﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Travix.Test.Common.Models;

namespace Travix.Test.Common.Repositories
{
    public class RepositoryBase<TDomain> : IRepository<TDomain>
        where TDomain : class, IEntity
    {
        private readonly DbSet<TDomain> _dbSet;

        public RepositoryBase(DbSet<TDomain> dbSet)
        {
            _dbSet = dbSet;
        }

        public DbSet<TDomain> All => _dbSet;

        public IRepository<TDomain> Add(TDomain entity)
        {
            _dbSet.Add(entity);
            return this;
        }

        public IRepository<TDomain> AddRange(IEnumerable<TDomain> entities)
        {
            _dbSet.AddRange(entities);
            return this;
        }

        public IRepository<TDomain> Remove(TDomain entity)
        {
            _dbSet.Remove(entity);
            return this;
        }

        public IRepository<TDomain> RemoveRange(IEnumerable<TDomain> entities)
        {
            _dbSet.RemoveRange(entities);
            return this;
        }

        public TDomain Find(params object[] keyValues)
        {
            return _dbSet.Find(keyValues);
        }

        public Task<TDomain> FindAsync(params object[] keyValues)
        {
            return _dbSet.FindAsync(keyValues);
        }

        public bool Any()
        {
            return _dbSet.Any();
        }

        public Task<bool> AnyAsync()
        {
            return _dbSet.AnyAsync();
        }

        public IQueryable<TDomain> FromSql(FormattableString sql)
        {
            return _dbSet.FromSql(sql);
        }

        public IQueryable<TDomain> FromSql(RawSqlString sql, params object[] parameters)
        {
            return _dbSet.FromSql(sql, parameters);
        }
    }
}
