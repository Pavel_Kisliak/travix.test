﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Travix.Test.Common.Models;

namespace Travix.Test.Common.Repositories
{
    public interface IRepository<TDomain>
        where TDomain : class, IEntity
    {
        DbSet<TDomain> All { get; }

        IRepository<TDomain> Add(TDomain entity);
        IRepository<TDomain> AddRange(IEnumerable<TDomain> entities);
        IRepository<TDomain> Remove(TDomain entity);
        IRepository<TDomain> RemoveRange(IEnumerable<TDomain> entities);
        TDomain Find(params object[] keyValues);
        Task<TDomain> FindAsync(params object[] keyValues);
        bool Any();
        Task<bool> AnyAsync();
        IQueryable<TDomain> FromSql(FormattableString sql);
        IQueryable<TDomain> FromSql(RawSqlString sql, params object[] parameters);
    }
}
