﻿using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Travix.Test.Common.Models;

namespace Travix.Test.Common.Repositories
{
    public abstract class UnitOfWorkBase : DbContext, IUnitOfWorkBase
    {
        protected UnitOfWorkBase(DbContextOptions options)
            : base(options)
        {
        }

        public virtual int Save()
        {
            return SaveChanges();
        }

        public virtual Task<int> SaveAsync()
        {
            return SaveChangesAsync();
        }

        public bool HasChanges()
        {
            return ChangeTracker.HasChanges();
        }

        private readonly ConcurrentDictionary<Type, object> _repositoryCache = new ConcurrentDictionary<Type, object>();

        public IRepository<TDomain> GetRepository<TDomain>()
            where TDomain : class, IEntity
        {
            return
                (RepositoryBase<TDomain>)
                _repositoryCache.GetOrAdd(typeof(TDomain), new RepositoryBase<TDomain>(Set<TDomain>()));
        }
    }
}
