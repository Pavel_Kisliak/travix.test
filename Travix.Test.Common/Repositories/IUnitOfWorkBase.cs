﻿using System;
using System.Threading.Tasks;
using Travix.Test.Common.Models;

namespace Travix.Test.Common.Repositories
{
    public interface IUnitOfWorkBase : IDisposable
    {
        bool HasChanges();
        int Save();
        Task<int> SaveAsync();
        IRepository<TDomain> GetRepository<TDomain>() where TDomain : class, IEntity;
    }
}
