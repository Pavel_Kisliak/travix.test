﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Travix.Test.Common
{
    /// <summary>
    /// Interface that define bootstrapping for library
    /// </summary>
    public interface IBootstrapper
    {
        /// <summary>
        /// Initializes the specified container.
        /// </summary>
        /// <param name="services">The container.</param>
        /// <param name="configuration">The configuration.</param>
        void ConfigureServices(IServiceCollection services, IConfiguration configuration);
    }
}
