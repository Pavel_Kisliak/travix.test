﻿using System;
using System.Net;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Travix.Test.Common.Exceptions.Api;

namespace Travix.Test.Common.ActionFilters
{
    /// <summary>
    /// This filter processing API exceptions related to business logic (e.g: entity is not found) and produces an HTTP response with suitable status code.
    /// </summary>
    public class ApiExceptionsFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            var statusCode = GetExceptionHttpStatusCode(context.Exception);
            if (statusCode == HttpStatusCode.InternalServerError)
                base.OnException(context);
            else
            {
                context.ExceptionHandled = true;
                context.Result = new ObjectResult(context.Exception.Message)
                {
                    StatusCode = (int) statusCode
                };
            }
        }

        private static HttpStatusCode GetExceptionHttpStatusCode(Exception exception)
        {
            if (exception is NotFoundException)
            {
                return HttpStatusCode.NotFound;
            }
            // With it's inheritors - ArgumentNullException, ArgumentOutOfRangeException
            if (exception is ArgumentException)
            {
                return HttpStatusCode.BadRequest;
            }
            if (exception is ConflictException)
            {
                return HttpStatusCode.Conflict;
            }
            if (exception is UnauthorizedAccessException)
            {
                return HttpStatusCode.Forbidden;
            }
            if (exception is TimeoutException)
            {
                return HttpStatusCode.RequestTimeout;
            }
            return HttpStatusCode.InternalServerError;
        }
    }
}
