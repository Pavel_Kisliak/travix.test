﻿using System;
using System.Linq;
using AutoMapper;

namespace Travix.Test.Common.TypeMapping
{
    public class Mapper : IMapper
    {
        private readonly AutoMapper.IMapper _innerMapper;

        public Mapper()
        {
            var config = GetMapperConfiguration();
            _innerMapper = config.CreateMapper();
        }

        public TDest Map<TDest>(object src)
        {
            return _innerMapper.Map<TDest>(src);
        }

        public TDest Map<TDest>(object src, TDest dst)
        {
            return _innerMapper.Map(src, dst);
        }

        private static MapperConfiguration GetMapperConfiguration()
        {
            var mapProfiles = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(s => s.GetTypes())
                .Where(t => !t.IsAbstract && !t.IsInterface)
                .Where(t => t.IsSubclassOf(typeof(MapProfileBase)))
                .ToList();
            return new MapperConfiguration(cfg => mapProfiles.ForEach(cfg.AddProfile));
        }
    }
}
