﻿namespace Travix.Test.Common.TypeMapping
{
    public interface IMapper
    {
        TDest Map<TDest>(object src);
        TDest Map<TDest>(object src, TDest dst);
    }
}
