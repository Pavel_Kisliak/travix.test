﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Travix.Test.Business.Models;
using Travix.Test.Business.Providers.Interfaces;
using Travix.Test.Common.ActionFilters;

namespace Travix.Test.Controllers
{
    // From my point of view, controllers should be very thin (no any business logic),
    // just return result of action from provider or service.
    // If we will need to do kind of new server, for example based on SignalR, it will be not a problem, we wouldn't duplicate code.

    // So, regarding it, I think that is no needs to write unit tests for just test how works model validation
    // (may be for more complicated case), it will be "tests for the sake of tests".

    [Produces("application/json")]
    [Route("api/posts")]
    public class PostsController : Controller
    {
        private readonly IPostsProvider _postsProvider;

        public PostsController(IPostsProvider postsProvider)
        {
            _postsProvider = postsProvider;
        }

        // GET api/posts
        [HttpGet]
        [ProducesResponseType(typeof(List<Post>), (int)HttpStatusCode.OK)]
        public Task<IList<Post>> GetAll()
        {
            return _postsProvider.GetAllPosts();
        }

        // GET api/posts/5
        [HttpGet("{id:int}")]
        [ProducesResponseType(typeof(Post), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            var item = await _postsProvider.GetPost(id);
            if (item == null)
                return NotFound();
            return new ObjectResult(item);
        }

        // POST api/posts
        [HttpPost]
        [ValidateModel]
        [ProducesResponseType(typeof(Post), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create([FromBody][Required]Post post)
        {
            var result = await _postsProvider.Create(post);
            // Adds a Location header to the response. The Location header specifies the URI of the newly created to-do item.
            return CreatedAtAction(nameof(GetById), new { id = result.PostId }, result);
        }

        // PUT api/posts/5
        [HttpPut("{id:int}")]
        [ValidateModel]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update(int id, [FromBody]Post post)
        {
            if (post == null || post.PostId != id)
                return BadRequest();

            await _postsProvider.Update(post);
            // The server successfully processed the request, but is not returning any content
            return new NoContentResult();
        }

        // DELETE api/posts/5
        [HttpDelete("{id:int}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            await _postsProvider.Delete(id);
            // Action has been enacted but the response does not include an entity.
            return new NoContentResult();
        }
    }
}