﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Travix.Test.Business.Models;
using Travix.Test.Business.Providers.Interfaces;
using Travix.Test.Common.ActionFilters;

namespace Travix.Test.Controllers
{
    // From my point of view, controllers should be very thin (no any business logic),
    // just return result of action from provider or service.
    // If we will need to do kind of new server, for example based on SignalR, it will be not a problem, we wouldn't duplicate code.

    // So, regarding it, I think that is no needs to write unit tests for just test how works model validation
    // (may be for more complicated case), it will be "tests for the sake of tests".

    [Produces("application/json")]
    [Route("api/posts")]
    public class CommentsController : Controller
    {
        private readonly ICommentsProvider _commentsProvider;

        public CommentsController(ICommentsProvider commentsProvider)
        {
            _commentsProvider = commentsProvider;
        }

        // GET api/posts/5/comments
        [HttpGet("{postId:int}/comments")]
        [ProducesResponseType(typeof(List<Comment>), (int)HttpStatusCode.OK)]
        public Task<IList<Comment>> GetAllCommentsForPost(int postId)
        {
            return _commentsProvider.GetAllCommentsForPost(postId);
        }

        // GET api/posts/5/comments/3
        [HttpGet("{postId:int}/comments/{id:int}")]
        [ProducesResponseType(typeof(Comment), (int)HttpStatusCode.OK)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> GetById(int postId, int id)
        {
            var item = await _commentsProvider.GetComment(id);
            if (item == null)
                return NotFound();
            return new ObjectResult(item);
        }

        // POST api/posts/5/comments
        [HttpPost("{postId:int}/comments")]
        [ValidateModel]
        [ProducesResponseType(typeof(Comment), (int)HttpStatusCode.Created)]
        [ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Create(int postId, [FromBody]Comment comment)
        {
            if (comment == null || comment.PostId != postId)
                return BadRequest();

            var result = await _commentsProvider.Create(comment);
            // Adds a Location header to the response. The Location header specifies the URI of the newly created to-do item.
            return CreatedAtAction(nameof(GetById), new { postId = result.PostId, id = result.CommentId }, result);
        }

        // PUT api/posts/5/comments/3
        [HttpPut("{postId:int}/comments/{id:int}")]
        [ValidateModel]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        [ProducesResponseType(typeof(ModelStateDictionary), (int)HttpStatusCode.BadRequest)]
        public async Task<IActionResult> Update(int postId, int id, [FromBody]Comment comment)
        {
            if (comment == null || comment.CommentId != id || comment.PostId != postId)
                return BadRequest();

            await _commentsProvider.Update(comment);
            // The server successfully processed the request, but is not returning any content
            return new NoContentResult();
        }

        // DELETE api/posts/5/comments/3
        [HttpDelete("{postId:int}/comments/{id:int}")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.NotFound)]
        public async Task<IActionResult> Delete(int postId, int id)
        {
            await _commentsProvider.Delete(id);
            // Action has been enacted but the response does not include an entity.
            return new NoContentResult();
        }
    }
}