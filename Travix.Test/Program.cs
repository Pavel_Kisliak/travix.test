﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using NLog;
using NLog.Extensions.Logging;
using NLog.Web;
using Travix.Test.Data;
using Travix.Test.Data.UnitOfWork;

namespace Travix.Test
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // NLog: setup the logger first to catch all errors
            var logger = NLogBuilder.ConfigureNLog("NLog.config").GetCurrentClassLogger();
            logger.Info("Start application");
            try
            {
                logger.Debug("Build Web host");
                var host = BuildWebHost(args);

                logger.Debug("Initialize database");
                InitDatabase(host, logger);

                logger.Debug("Run Web host");
                host.Run();
            }
            catch (Exception e)
            {
                logger.Error(e, "Stopped program because of exception");
                throw;
            }
            logger.Info("Stop application");
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            // CreateDefaultBuilder loads optional configuration from appsettings.json, appsettings.{Environment}.json,
            // user secrets (in the Development environment), environment variables, and command-line arguments.
            return WebHost.CreateDefaultBuilder(args)
                .ConfigureLogging((hostingContext, logging) =>
                {
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                    logging.AddConsole();
                    logging.AddNLog();
                })
                .UseStartup<Startup>()
                .Build();
        }

        private static void InitDatabase(IWebHost host, Logger logger)
        {
            using (var scope = host.Services.CreateScope())
            {
                var services = scope.ServiceProvider;
                try
                {
                    var context = services.GetRequiredService<TestUnitOfWork>();
                    var environment = services.GetRequiredService<IHostingEnvironment>();
                    DbInitializer.Initialize(context, environment);
                }
                catch (Exception ex)
                {
                    logger.Error(ex, "An error occurred while seeding the database.");
                    throw;
                }
            }
        }
    }
}
